<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create( [
            'id'=>1,
            'name'=>'Administrator',
            'email'=>'admin@test.com',
            'password'=>bcrypt('qweqwe')
        ] );
    }
}
