@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="my-3 float-right">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Add Site</button>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form class="modal-body">
                        <div class="alert d-none" role="alert"></div>
                        <div class="form-group">
                            <label for="name">Name of Website</label>
                            <input type="text" class="form-control" id="name" aria-describedby="name" placeholder="Site's Name">
                        </div>
                        <div class="form-group">
                            <label for="url">URL of Website</label>
                            <input type="text" class="form-control" id="url" aria-describedby="url" placeholder="Site's URL">
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="addSite();" id="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
 <div class="my-2 float-left">
            <div class="form-inline">
                <div class="form-group mx-sm-3 mb-2">                    
                    <label for="daterange" class="mx-2">Selected Dates:</label>
                    <input class="form-control" type="text" id="daterange" name="daterange" value="01/01/2018 - 01/15/2018" />
                </div>
                <button type="submit" class="btn btn-primary mb-2" onclick="getStats();">Submit</button>
            </div>
        </div>

    <table class="table table-bordered" style="text-align: center;">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Site Name</th>
                <th scope="col">Number of Visits</th>
                <th scope="col">Tool</th>

            </tr>
        </thead>
        <tbody >
            @if(count($sites)>0)
                @foreach($sites as $site)
                    <tr >
                        <th scope="row"><a href="#" onclick="viewSite({{$site->id}});">{{$site->site_name}}</a></th>
                        @php
                            $visit = DB::table('visits')->where('site_visited', $site->id)->count();
                            echo '<td id="what">'.$visit.'</td>';

                        @endphp
                        <td>
                            <a href="/admin/sites/{{$site->id}}" class="btn btn-info"><i class="fas fa-chart-bar" ></i></a>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#siteModal{{$site->id}}"><i class="far fa-trash-alt"></i></button>

                            <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" id="siteModal{{$site->id}}">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Delete Site</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure you want to remove <b>{{$site->site_name}}</b> from the list?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-danger" onclick="removeSite({{$site->id}});">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                     
                    </tr>
                   
                @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript">
    function viewSite(id){
        axios.get('/sites/'+id)
        .then(function(response){
            console.log(response);
            var MyPopUp = false;
            OpenWindow(response.data.url, response.data.site_name);
            function OpenWindow(url,name){
            //checks to see if window is open
                if(MyPopUp && !MyPopUp.closed){
                    winPop.focus(); //If already Open Set focus
                }
                else{
                    MyPopUp = window.open(url, "PopUp", "_blank");//Open a new PopUp window
                }
            }
            // console.log(response.data.url);
            // window.open(response.data.url, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=100%,height=100%").focus();
        })
        .catch(function(error){
            console.log(error);
        });
    }

    function removeSite(id){
        $('#removeSiteModal').modal('show');
        axios.get('/sites/remove/'+id)
        .then(function(res){
            if (res.data == 'deleted') {
                swal({
                    type: 'success',
                    title: 'Record Successfully removed.',
                    showConfirmButton: !1,
                    timer: 3000
                })
                setTimeout(function() {
                    $(location).attr('href', '{{url('/admin/sites')}}')
                }, 3000)
            }
        })
        .catch(function(err){
            console.log(err);
        });
    }

    function addSite(){
        if($('#name').val() == '' || $('#url').val() == ''){
            $('#save').attr('disabled','disabled');
            $('.alert').addClass('alert-danger').removeClass('d-none').text('Please fill all of the information.');
            setTimeout(function(){
                $('.alert').addClass('d-none').removeClass('alert-danger');
                $('#save').removeAttr('disabled');
            }, 3000);
        }else{
            let form = new FormData();
            form.append('_token', $('input[name=_token]').val());
            form.append('name', $('#name').val());
            form.append('url', $('#url').val());

            axios.post('/sites/store', form)
            .then(function(response){
                if(response.data == 'success'){
                    $('.alert').addClass('alert-success').removeClass('d-none').text('Record saved.');
                    setTimeout(function(){
                        location.reload();
                    }, 3000);
                }else{
                    $('.alert').addClass('alert-warning').removeClass('d-none').text('Max number of sites reached.');
                    setTimeout(function(){
                        $('.alert').addClass('d-none').removeClass('alert-warning');
                    }, 3000);
                }
            })
            .catch(function(error){
                console.log(error);
            });
        }
    }


    var startDate = moment().format('YYYY-MM-DD');
    var endDate = moment().format('YYYY-MM-DD');
    $('#daterange').val(moment().format('YYYY-MM-DD'));
    $(function() {
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            startDate = start.format('YYYY-MM-DD');
            endDate = end.format('YYYY-MM-DD');
        });
    });

    
    function getStats(){
        console.log(startDate+' - '+ endDate);
        
        let form = new FormData();
        form.append('start', startDate);
        form.append('end', endDate);
        form.append('gender', $('#gender').val());
        axios.post('/admin/sites/users', form)
        .then(function(res){
            console.log(res);
            $('#what').empty().append(res.data);
        })
        .catch(function(err){
            console.log(err);
        });

    }
</script>
@endsection