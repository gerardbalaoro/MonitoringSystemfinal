@extends('layouts.admin')

@section('content')<br><br><br>
<div class="container shadow-lg"><br>
    <div class="mx-3 my-3">
        <div class="my-2 float-left">
            <div class="form-inline">
                <div class="form-group mb-2">
                    <label for="gender" class="mx-2">Gender:</label>
                    <select class="form-control" id="gender">
                        <option value="all">All</option>
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                </div>
                <div class="form-group mx-sm-3 mb-2">                    
                    <label for="daterange" class="mx-2">Selected Dates:</label>
                    <input class="form-control" type="text" id="daterange" name="daterange" value="01/01/2018 - 01/15/2018" />
                </div>
                <button type="submit" class="btn btn-primary mb-2" onclick="getStats();">Submit</button>
            </div>
        </div>

        <table class="table table-bordered" style="text-align: center;">
            <thead class="thead-dark">
                <tr>
                    <th scope="col" style="width:150px;"></th>
                    <th scope="col">Prep</th>
                    <th scope="col">Elementary</th>
                    <th scope="col">JHS</th>
                    <th scope="col">SHS</th>
                    <th scope="col">College</th>
                    <th scope="col">Professional</th>
                    <th scope="col">Senior Cetizen</th>
                    <th scope="col">OSY</th>
                    <th scope="col">PWD</th>
                     <th scope="col">Total</th>
                   
                   
                    

                </tr>
            </thead>
            <tbody id="tblLogs">
                @if(count($schedules)>0)
                    @foreach($schedules as $schedule)
                        <tr>
                            <th scope="row">{{date('h:i A', strtotime($schedule->start_period))}} - {{date('h:i A', strtotime($schedule->end_period))}}</th>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                              <td>0</td>

                         
                        </tr>

                    @endforeach
                    <tr class="table-secondary">
                        <th scope="row">Total</th>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                          <td>0</td>
                    
                     
                    </tr>

                   
                @endif
                    
                   
              
            </tbody>
        </table>
    </div>
    <br>
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">



	var startDate = moment().format('YYYY-MM-DD');
	var endDate = moment().format('YYYY-MM-DD');
	$('#daterange').val(moment().format('YYYY-MM-DD'));
	$(function() {
		$('input[name="daterange"]').daterangepicker({
			opens: 'left'
		}, function(start, end, label) {
			console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
			startDate = start.format('YYYY-MM-DD');
			endDate = end.format('YYYY-MM-DD');
		});
	});

    
	function getStats(){
		console.log(startDate+' - '+ endDate);
		
		let form = new FormData();
		form.append('start', startDate);
		form.append('end', endDate);
        form.append('gender', $('#gender').val());
		axios.post('/admin/logs/users', form)
		.then(function(res){
            console.log(res);
            $('#tblLogs').empty().append(res.data);
		})
		.catch(function(err){
			console.log(err);
		});

	}
</script>
@endsection