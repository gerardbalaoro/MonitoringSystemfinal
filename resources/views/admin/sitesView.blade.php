@extends('layouts.admin')

@section('style')
@endsection

@section('content')
<div class="container text-center py-5 px-5">
	<div class="row">
		<div class="col-sm-12 col-md-8 col-lg-8">
			@if($site)
			<div id="piechart" style="width: 900px; height: 500px;"></div>
			@endif
		</div>
		<div class="col-sm-12 col-md-4 col-lg-4">
			<div class="form-group col-sm-12 col-md-12 col-lg-12">
				<label for="gender">Gender</label>
				<select class="form-control" id="gender">
					<option value="all">All</option>
					<option value="M">Male</option>
					<option value="F">Female</option>
				</select>
			</div>
			<div class="form-group col-sm-12 col-md-12 col-lg-12">
				<label for="educ">Educational Background</label>
				<select class="form-control" id="educ">
					<option value="all">All</option>
					<option>Prep</option>
					<option>Elementary</option>
					<option>Junior High School</option>
					<option>Senior High School</option>
					<option>College</option>
					<option>Professional</option>
					<option>Senior Citizen</option>
					<option>OSY</option>
					<option>PWD</option>
				</select>
			</div>
			<div class="form-group col-sm-12 col-md-12 col-lg-12">
				<label for="daterange">Selected Dates</label>
				<input class="form-control" type="text" id="daterange" name="daterange" value="01/01/2018 - 01/15/2018" />
			</div>
			<div class="form-group col-sm-12 col-md-12 col-lg-12">
				<button type="submit" class="btn btn-primary" onclick="baseGraph();">Submit</button>
			</div>
		</div>
	</div>	
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript">
	var startDate = '';
	var endDate = '';
	// console.log(moment().format('YYYY-MM-DD'));
	$('#daterange').val(moment().format('YYYY-MM-DD'));
	$(function() {
		$('input[name="daterange"]').daterangepicker({
			opens: 'left'
		}, function(start, end, label) {
			console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
			startDate = start.format('YYYY-MM-DD');
			endDate = end.format('YYYY-MM-DD');
		});
	});

	function baseGraph(){
		console.log(startDate+' - '+ endDate);
		var id = '<?php echo $id ?>';

		let form = new FormData();
		form.append('id', id);
		form.append('gender', $('#gender').val());
		form.append('educ', $('#educ').val());
		form.append('start', startDate);
		form.append('end', endDate);

		axios.post('/admin/sites/info', form)
		.then(function(response){
			var info = response.data;
			// console.log(info);
			console.log(response);

			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(drawChart);

			function drawChart() {

				var data = new google.visualization.DataTable();
	            data.addColumn('string', 'Site Name');
	            data.addColumn('number', 'Number of Visits');

	            for (var i = 0; i < info.length; i++) {
	                data.addRow([info[i][0], info[i][1]]);
	            }

	            // console.log(data);
	            var options = {
	                title: 'Number of Visits According to Gender'
	            };

				var chart = new google.visualization.PieChart(document.getElementById('piechart'));

				chart.draw(data, options);
			}
		})
		.catch(function(error){
			console.log(error);
		});
	}
	
	baseGraph();
</script>
@endsection