@extends('layouts.admin')

@section('content')
<div class="container">
	<div class="row">
		<div class="my-3 col-md-12">
			<div class="card shadow">
				<div class="card-header">
					User List
				</div>
				 
				<div class="card-body">
					<!-- <div class="float-right my-2">
						<label for="daterange">Selected Dates</label>
						<input class="form-control" type="text" id="daterange" onchange="getStats();" name="daterange" value="01/01/2018 - 01/15/2018" />
					</div> -->
					<table class="table table-bordered">
						<thead style="text-align: center;">
							<tr class="thead-dark">
								<th scope="col"></th>
								<th scope="col">Prep</th>
								<th scope="col">Elementary</th>
								<th scope="col">Junior High School</th>
								<th scope="col">Senior High School</th>
								<th scope="col">College</th>
								<th scope="col">Professional</th>
								<th scope="col">Senior Citizens</th>
								<th scope="col">OSY</th>
								<th scope="col">PWD</th>
                <th scope="col">Total</th>
              </tr>
							</tr>
						</thead>
						<tbody style="text-align: center;">
							<tr>
								<th scope="row" >Male</th>
								@if(count($male)>0)
									@foreach($male as $m)
										<td>{{$m}}</td>
									@endforeach
								@endif
							</tr>
							<tr>
								<th scope="row">Female</th>
								@if(count($female)>0)
									@foreach($female as $f)
										<td>{{$f}}</td>
									@endforeach
								@endif
							</tr>
							<tr class="table-secondary">
								<th scope="row">Total</th>
								@if(count($total)>0)
									@foreach($total as $t)
										<td>{{$t}}</td>
									@endforeach
								@endif
							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="card shadow" ><br><br>
			<input class="form-control form-control-lg right"  type="text" placeholder="Search..." style="width: 500px; position: absolute; right: 1.8%; margin-top: 1%; font-size: 12pt;" >
				<div class="card-body">
					<table id="users-table" class="table table-bordered">
						<thead style="text-align: center;">
							<tr class="thead-dark">
								<!-- <th scope="col">#</th> -->
								<th scope="col">ID</th>
								<th scope="col">Name</th>
								<th scope="col">Gender</th>
								<th scope="col">Educational Background</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody style="text-align: center;">
							@if(count($users) > 0 )
								@foreach($users as $user)
									<tr>
										<!-- <th scope="row">1</th> -->
										<td>{{$user->id}}</td>
										<td>{{$user->name}}</td>
										<td>{{$user->gender}}</td>
										<td>{{$user->education}}</td>
										<td> 
										 
                             <button type="button" class="btn btn-info" data-toggle="modal" data-target="#infoModal{{$user->id}}"><i class="fas fa-info-circle"></i></button>

                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#qrModal{{$user->id}}"><i class="fas fa-qrcode"></i></button>

                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#siteModal{{$user->id}}"><i class="far fa-trash-alt"></i></button>

                            <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" id="siteModal{{$user->id}}">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Delete Site</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure you want to remove <b>{{$user->name}}</b> from the list?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-danger" onclick="removeSite({{$user->id}});">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
								<div class="modal fade"  id="qrModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Your QR Code</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-center">
                                             <img id="myqr{{$user->id}}" class="qrcode" src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->color(38, 38, 38, 0.85)->backgroundColor(255, 255, 255, 0.82)->size(200)->generate($user->QRpassword)) !!}">
                                        </div>
                                        <div class="modal-footer" >
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <a href="#" class="btn btn-primary" onclick="this.href = $('#myqr{{$user->id}}').attr('src');" download="{{$user->name}}" id="{{$user->id}}">Download</a>
                                        </div>
                                        </div>
                                    </div>
                                </div>





								<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="infoModal{{$user->id}}">
  								<div class="modal-dialog modal-lg">
    								 <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Profile Information</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                          <div class="text-center">
 											                     @if($user->profilePicture)
                                          
                                            <img class="card-img-top rounded-circle shadow" src="/img/user_profile/{{$user->profilePicture}}" alt="Profile Picture" style="width:230px; height: 230px;">
                                            @else
                                            <img class="card-img-top rounded-circle shadow" src="/img/user_profile/we.jpg" alt="Profile Picture" style="width:230px; height: 230px;"></center>

                                            @endif
										  </div><br><br><center>
                        <label for="picture"><b>Profile Picture</b></label>
                      <div class="col-md-6 mb-3">
                         <div class="form-group ">
                                                  
                                                   
                                                </div>
                      </div></center>
										   <fieldset disabled>
										  <div class="form-row" style="margin-left: 20px; margin-right: 20px;">

 <div class="col-md-1 mb-3">
      <label for="validationDefault01"><b>ID</b></label>
      <input type="text" class="form-control is-valid" id="validationDefault01" placeholder="Name" value="{{$user->id}}" >

    </div>
    <div class="col-md-4 mb-3">
      <label for="validationDefault01"><b>Name</b></label>
      <input type="text" class="form-control is-valid" id="validationDefault01" placeholder="Name" value="{{$user->name}}" >

    </div>
    <div class="col-md-3 mb-3">
      <label for="validationDefault02"><b>Username</b></label>
      <input type="text" class="form-control is-valid" id="validationDefault02" placeholder="Email" value="{{$user->username}}" >
    </div>
    <div class="col-md-2 mb-3">
      <label for="validationDefault02"><b>Date of Birth</b></label>
      <input type="text" class="form-control is-valid" id="validationDefault02" placeholder="Birthdate" value="{{$user->date_of_birth}}" >
    </div>
   <div class="col-md-1 mb-3">
      <label for="validationDefault02"><b>Gender</b></label>
      <input type="text" class="form-control is-valid" id="validationDefault02" placeholder="Genter" value="{{$user->gender}}" >
    </div>
    <div class="col-md-1 mb-3">
      <label for="validationDefault02"><b>Age</b></label>
      <input type="text" class="form-control is-valid" id="validationDefault02" placeholder="" value="{{$user->age}}" >
    </div>
  </div>
  
  <div class="form-row" style="margin-left: 20px; margin-right: 20px;">
    <div class="col-md-4 mb-3">

      <label for="validationDefault03"><b>Email Address</b></label>
      <input type="text" class="form-control is-valid" id="validationDefault03" placeholder="" value="{{$user->email}}">

    </div>
     <div class="col-md-4 mb-3">
      <label for="validationDefault03"><b>Address</b></label>
      <input type="text" class="form-control is-valid" id="validationDefault03" placeholder="" value="{{$user->address}}">
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationDefault03"><b>Educational Background</b></label>
      <input type="text" class="form-control is-valid" id="validationDefault03" placeholder="" value="{{$user->education}}">
    </div>
  </div>
  <br>

 </fieldset>

  
                                        <div class="modal-footer">
                                         
                                           <button type="button" class="btn btn-danger" data-dismiss="modal" >Close</button>
                                          
                                        </div>
                                    </div>
                                     </div>
                                   </div>

                           
                            </td>
									</tr>
								@endforeach
							@endif
						</tbody>
					</table>
					<div class="float-right">
						{{ $users->links() }}
					</div>
				</div>
			</div>
		</div>
		 					

                        


	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">

    function removeSite(id){
        $('#removeSiteModal').modal('show');
        axios.get('/users/remove/'+id)
        .then(function(res){
            if (res.data == 'deleted') {
                swal({
                    type: 'success',
                    title: 'Record Successfully removed.',
                    showConfirmButton: !1,
                    timer: 3000
                })
                setTimeout(function() {
                    $(location).attr('href', '{{url('/admin/users')}}')
                }, 3000)
            }
        })
        .catch(function(err){
            console.log(err);
        });
    }

   
</script>
@endsection