<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Logs;
use App\Model\Sites;
use App\Model\Visits;
use App\User;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sites = Sites::all();
        $visits = array();
        foreach($sites as $site){
            $count = Visits::where('site_visited', $site->id)->count();
            array_push($visits, array($site->site_name, $count));
        }
		return view('admin.index');
    }

    public function getVisits(){
        $sites = Sites::all();
        $visits = array();
        foreach($sites as $site){
            $count = Visits::where('site_visited', $site->id)->count();
            array_push($visits, array($site->site_name, $count));
        }
        return response()->json($visits);

    }

 function education()
    {
     $data = DB::table('users')
       ->select(
        DB::raw('education as education'),
        DB::raw('count(*) as number'))
       ->groupBy('education')
       ->get();
     $array[] = ['education', 'Number'];
     foreach($data as $key => $value)
     {
      $array[++$key] = [$value->education, $value->number];
     }
     return view('admin.index')->with('education', json_encode($array));
    }
  


}
