<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Terminal;

class TerminalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

	public function index(){
		$terminals = Terminal::all();
		return view('admin.terminal')->with(compact('terminals'));
	}

	public function store(Request $request){
		$input = $request->all();
		try{
            DB::beginTransaction();
			$terminal = new Terminal;
			$terminal->terminal_number = $input['terminalNumber'];
			$terminal->ip_address = $input['terminalIP'];
            $terminal->save();
            DB::commit();
            return 'success';
        }catch (\Throwable $e) {
           DB::rollback();
           throw $e;
        }
	}
}
