<?php

namespace App\Http\Controllers;

use App\Events\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;
use Sentinel;
use App\User;


class QRController extends Controller
{

    public function __construct()
    {
		$this->middleware('auth', ['except' => ['index', 'checkUser', 'checkTimein']]);
    }

	public function index(){
		return view('auth.QRlogin');
	}

	public function checkUser(Request $request){
		$result =0;
		$input = $request->all();
		if($input['code']){
			$user = User::where('QRpassword', $input['code'])->first();
			if($user){
				Auth::login($user);
        		event(new Login($user));
				$result = 1;
			}else{
				$result = 0;
			}
		}
		return $result;
	}

	public function checkTimein(Request $request){
		$result =0;
		$input = $request->all();
		if($input['code']){
			$user = User::where('QRpassword', $input['code'])->first();
			if($user){
        		event(new Login($user));
				$result = 1;
			}else{
				$result = 0;
			}
		}
		return $result;
	}
}
