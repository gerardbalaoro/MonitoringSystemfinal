<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Model\Schedule;
use App\Model\Logs;
use App\User;
use App\Section;
use App\UserLog;


class LogsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){      
        $logs = DB::table('user_logs')
            ->join('users', 'user_logs.user_id', '=', 'users.id')
            ->select('user_logs.*','users.name')
            ->orderBy('id', 'asc')
            ->get();
            // $today = date('Y-m-d H:i:s');
        $today = date('Y-m-d');
        $today = Logs::distinct()->whereDate('login_time', $today)->get(['user_id', 'login_time']);
        // return $today;
        // dd($logs);
        $schedules = Schedule::all();
        return view('admin.logs')->with(compact('logs', 'schedules'));
    }

    public function getUsersLogs(Request $request){
        $input = $request->all();
        // return $input;

        $start = $input['start'];
        $end = $input['end'];
        $gender = $input['gender'];
        
        $logs = DB::table('user_logs')
            ->join('users', 'user_logs.user_id', '=', 'users.id')
            ->select('user_logs.*','users.name','users.education')
            ->whereDate('login_time', '>=', $input['start'])
            ->whereDate('login_time', '<=', $input['end'])
            ->whereTime('login_time', '>=', '14:00:00')
            ->whereTime('login_time', '<=', '15:00:00')
            ->where('gender', 'M')
            ->orderBy('id', 'asc')
            ->count();
        // return $logs;
        $schedules = Schedule::all();
        
        return View::make('admin.tblLogs')->with(compact('schedules', 'start', 'end', 'gender'))->render();
    }

    public function computer(Request $request, Section $section) {

        // Get start_date and end_date from request, use today if unset
        $start_date = date_format(date_create($request->start ?: 'today'), 'Y-m-d');
        $end_date = date_format(date_create($request->end ?: 'today'), 'Y-m-d');

        // Logs data
        $logs = ['by_time' => [], 'by_user' => [], 'start'];
        $form = $request->all();

        // Populate logs by user education
        foreach (User::distinct()->get(['education'])->pluck('education') as $type) {
            $query = UserLog::whereDate('created_at', '>=' , $start_date)->whereDate('created_at', '<=', $end_date);

            $logs['by_user'][$type] = $query->whereHas('user', function ($q) use ($type, $request) {
                $q->where('education', '=', $type);
                if ($request->gender) $q->where('gender', '=', $request->gender);
            })->get();
        }

        // Populate logs by time slot
        foreach (Schedule::all() as $schedule) {

            // Prepare query
            $query = UserLog::whereDate('created_at', '>=' , $start_date)->whereDate('created_at', '<=', $end_date)->whereTime('created_at', '>=', $schedule->start_period)->whereTime('created_at', '<=', $schedule->end_period);
            $query = $query->whereHas('user', function ($q) use ($request) {
                if ($request->gender) $q->where('gender', '=', $request->gender);
            });

            // Append to $logs
            $logs['by_time'][] = (object) [
                'start' => $schedule->start_period,
                'end' => $schedule->end_period,
                'items' => $query->get()
            ];                
        }

        return view('admin.stats.logs')->with(compact('logs', 'form'));
    }
}
